// Implemente a função removeProperty, que recebe um objeto e o nome de uma propriedade.

// Faça o seguinte:

// Se o objeto obj tiver uma propriedade prop, a função removerá a propriedade do objeto e retornará true;
// em todos os outros casos, retorna falso.

function removeProperty(obj, prop){
   if (obj.hasOwnProperty(prop)){//verifica se o obj tem a propriedade prop retorna true or false
     delete (obj[prop]);
     return true;
   }
   else{
     return false;
   }
}