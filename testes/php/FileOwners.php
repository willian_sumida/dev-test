<?php


/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Implemente uma função que ao receber um array associativo contendo arquivos e donos, retorne os arquivos agrupados por dono. 

Por exempolo, um array ["Input.txt" => "Jose", "Code.py" => "Joao", "Output.txt" => "Jose"] a função groupByOwners deveria retornar ["Jose" => ["Input.txt", "Output.txt"], "Joao" => ["Code.py"]].


*/

class FileOwners
{
    public static function groupByOwners($files)
    {
        $arr = array();
        $jose = array();
        $joao = array();

        foreach($files as $key => $value){
            if ($value == "Jose"){
               array_push($jose, $key);
            }
            if ($value == "Joao"){
                array_push($joao, $key);
            }
        }
        $arr["Jose"] = $jose;
        $arr["Joao"] = $joao;

        return($arr);
    
    }
}

$files = array
(
    "Input.txt" => "Jose",
    "Code.py" => "Joao",
    "Output.txt" => "Jose",
    "Click.js" => "Maria",
    "Out.php" => "maria",
);
var_dump(FileOwners::groupByOwners($files));

?>